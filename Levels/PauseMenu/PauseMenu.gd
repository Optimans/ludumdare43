extends CanvasLayer

onready var windows_opened = 0

func _ready():
    $C.hide()

func _input(event):
    if Input.is_action_just_pressed("pause"):
        if get_tree().paused:
            if windows_opened > 0:
                windows_opened -= 1
                return
            $C.hide()
            get_tree().paused = false
        else:
            $C/VBox/Surrender.grab_focus()
            $C.show()
            get_tree().paused = true

func _on_Exit_Game_pressed():
    $QuitConfirm.popup()


func _on_QuitConfirm_confirmed():
    get_tree().quit()


func _on_Surrender_pressed():
    $SurrenderConfirm.popup()


func _on_SurrenderConfirm_confirmed():
    get_tree().paused = false
    get_tree().change_scene(global.current_level)
