extends Node

export (int) var players_count
const player_scene = preload("res://Player/Player.tscn")
const PLAYERS_OFFSET = 100
export (String, FILE, "*.tscn") var next_level
var max_key_required


func _ready():
    for i in range(players_count):
        var new_sprite = TextureRect.new()
        new_sprite.texture = load("res://Environment/lives_1.png")
#        new_sprite. = Vector2(3, 3)
        $CanvasLayer/PanelContainer/VBox/People.add_child(new_sprite)
#    $CanvasLayer/VBox/People.text = "People Left: " + str(players_count)
    global.controlling_char = 0
    for i in range(players_count):
        var player = player_scene.instance()
        player.position = $PlayersOrigin.position - Vector2(i * PLAYERS_OFFSET, 0)
        player.connect("died", self, "_player_died")
        player.idx = i
        $Players.add_child(player)
    $Players.get_child(0).set_active()
    $Players.get_child(0).set_activity(false)
    _connect_group("keys", "key_gathered", "_player_got_key")
    $Exit.keys_required = get_tree().get_nodes_in_group("keys").size()
    max_key_required = $Exit.keys_required
    for i in range($Exit.keys_required):
        var new_sprite = TextureRect.new()
        new_sprite.texture = load("res://Environment/keyWhole_1.png")
#        new_sprite.scale = Vector2(3, 3)
        $CanvasLayer/PanelContainer/VBox/Keys.add_child(new_sprite)
#    $CanvasLayer/VBox/Switches.text = "Switches Left: " + str($Exit.keys_required)
    $Exit.connect("player_exited", self, "_player_exited")
        
func _player_died():
    global.controlling_char += 1
    $CanvasLayer/PanelContainer/VBox/People.get_child(players_count - global.controlling_char).texture = load("res://Environment/lives_0.png")
#    $CanvasLayer/VBox/People.text = "People Left: " + str(players_count - global.controlling_char)
    if global.controlling_char == players_count:
        _show_game_over()
    else:
        $Players.get_child(global.controlling_char).set_active()
        
        
func _show_game_over():
    get_tree().quit()
    
    
func _player_got_key():
    $Exit.unlock_one()
#    $CanvasLayer/VBox/Switches.text = "Switches Left: " + str($Exit.keys_required)
    $CanvasLayer/PanelContainer/VBox/Keys.get_child(max_key_required - $Exit.keys_required - 1).texture = load("res://Environment/keyWhole_0.png")


func _connect_group(group_name, signal_name, callback_name):
    for item in get_tree().get_nodes_in_group(group_name):
        item.connect(signal_name, self, callback_name)


func _activate_player():
    $Players.get_child(0).set_activity(true)
    

func _player_exited():
    global.current_level = next_level
    $AnimationPlayer.play("ExitLevel")
    
func _go_to_next_scene():
    get_tree().change_scene(next_level)