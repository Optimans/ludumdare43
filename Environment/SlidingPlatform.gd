extends Node2D

export (Vector2) var target
const SPEED = 4
var player

func _ready():
    if target.x == 0:
        target.x = position.x
    if target.y == 0:
        target.y = position.y
    $Tween.interpolate_property(self, "position", 
            position, target,
            5.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)

func _on_Area2D_body_entered(body):
    add_child(body)
    player = body
    player.set_activity(false)
    $Tween.start()
    $CenterPlayer.interpolate_property(body, "global_position",
        body.global_position, $PlayerCentered.global_position,
        0.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
#    $CenterPlayer.start()


func _on_Tween_tween_completed(object, key):
    player.set_activity(true)
    $Area2D/CollisionShape2D.disabled = true
