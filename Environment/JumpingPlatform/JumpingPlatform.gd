extends Area2D

export (int) var jump_force = 1500

func _ready():
#    $Particles2D.show()
    pass


func _on_player_entered(body):
    var rotation_rad = deg2rad(rotation_degrees+90)
    if not body.is_on_floor():
        body.speed = -jump_force * Vector2(cos(rotation_rad), sin(rotation_rad))
    $AnimationPlayer.play("OnJump")
