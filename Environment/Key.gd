extends Area2D

signal key_gathered
export (int) var texture_idx = 0


func _ready():
    add_to_group("keys")
    match texture_idx:
        0:
            $RedStar.texture = load("res://Environment/keypiece_0.png")
        1:
            $RedStar.texture = load("res://Environment/keypiece_1.png")
        2:
            $RedStar.texture = load("res://Environment/keypiece_2.png")
            
            
    
    

func _on_Key_body_entered(body):
    emit_signal("key_gathered")
    queue_free()
