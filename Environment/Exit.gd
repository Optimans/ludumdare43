extends Node2D

signal player_exited

var keys_required

func _ready():
    pass


func unlock_one():
    keys_required -= 1
    $Label.text = str(keys_required)
    if keys_required == 0:
        $Wall/Shape.disabled = true
        $Sprite.hide()
    

func _on_ExitLine_body_entered(body):
    emit_signal("player_exited")
