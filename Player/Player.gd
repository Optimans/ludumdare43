extends KinematicBody2D

export (int) var idx
onready var speed = Vector2(0, 0)
const MAX_SPEED = 400
const GRAVITY = 2000
onready var accel = Vector2(0, GRAVITY)
onready var accel_y = 0
const JUMP_FORCE = GRAVITY * 0.4
onready var can_move = true
onready var jumped = false
const DECELL = 1200
const MAX_ACCEL = 2000
onready var prev_speed = Vector2(0, 0)
signal died
onready var dead = false


func _ready():
    $AnimatedSprite.hide()
    $Sprite.show()
    add_to_group("players")
    $SuicideProgress.hide()
    
    
func _physics_process(delta):
    if global.controlling_char != idx or dead:
        return
        
    prev_speed = speed
    if not is_on_floor():
        speed.y += GRAVITY * delta
    else:
        speed.y = 0
    _manage_input()
    move_and_slide(speed, Vector2(0, -1))
    
    if abs((speed - prev_speed).y) > 2000:
        die()
    
    
func _manage_input():
    if not can_move:
        return

    if Input.is_action_pressed("move_left"):
        speed.x = -MAX_SPEED
        $AnimatedSprite.playing = true
        $AnimatedSprite.show()
        $Sprite.hide()
        $AnimatedSprite.scale.x = -5.5
        $Sprite.scale.x = -5.5
        $AnimatedSprite.playing = true
    elif Input.is_action_pressed("move_right"):
        speed.x = MAX_SPEED
        $AnimatedSprite.playing = true
        $AnimatedSprite.show()
        $Sprite.hide()
        $AnimatedSprite.scale.x = 5.5
        $Sprite.scale.x = 5.5
    else:
        $AnimatedSprite.playing = false
        $AnimatedSprite.hide()
        $Sprite.show()
        speed.x = 0

    if Input.is_action_pressed("self_kill"):
        $SuicideProgress.show()
        $SuicideProgress.value += 2
        if $SuicideProgress.value == $SuicideProgress.max_value:
            $SuicideProgress.hide()
            $AnimationPlayer.play("DeathPill")
    else:
        $SuicideProgress.hide()
        $SuicideProgress.value = 0

    if Input.is_action_just_pressed("jump"):
        if is_on_floor() or speed.y < 50.0:
            _jump()

        
        
func _jump():
    speed.y = -JUMP_FORCE
    jumped = true


func set_activity(activity):
    can_move = activity
    speed = Vector2(0, 0)
    $Sprite.show()
    $AnimatedSprite.hide()
    $AnimatedSprite.playing = false


func set_active():
    set_physics_process(true)
    $Camera2D.current = true

    
func die():
    dead = true
    set_process(false)
    $AnimatedSprite.queue_free()
    $Sprite.queue_free()
    $DeathAnimation.show()
    $DeathAnimation.playing = true
    $AudioStreamPlayer2D.play()
    $AnimationPlayer2.play("Die")